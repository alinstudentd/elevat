import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ElevNou } from '../models/ElevNou';
import { Profile } from '../models/Profile';
import { AccountService } from '../sevices/account.service';
import { ActivatedRoute } from "@angular/router";
import { ElevServiceService } from '../sevices/elev-service.service';

@Component({
  selector: 'app-editare-elev',
  templateUrl: './editare-elev.component.html',
  styleUrls: ['./editare-elev.component.css']
})

export class EditareElevComponent implements OnInit {
  elevN: object = {};

  elevNou = new FormGroup({
    nume: new FormControl('',  Validators.required),
    prenume: new FormControl('', Validators.required),
    role: new FormControl('', Validators.required),
    varsta: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    clasa: new FormControl('', Validators.required),
    liceul: new FormControl('', Validators.required),
    telefon: new FormControl('', Validators.required),
    adresa: new FormControl('', Validators.required)
  });

  constructor(private accountService: AccountService,
              private router: Router,
              private elevService: ElevServiceService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
     const id = this.route.snapshot.paramMap.get('id') || undefined;
     if (id) {
      this.elevService.getElev(id)
      .subscribe( (profil ) => this.elevNou.patchValue(profil) )
     }

    }

  onSubmit() {
    const id = this.route.snapshot.paramMap.get('id') || undefined;
    this.elevNou.value.id = id;
    this.elevService.updateElev(this.elevNou.value);
    this.router.navigate(['/elevi'])
  }
}
