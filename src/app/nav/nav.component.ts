import { Component, OnInit } from '@angular/core';
import { delay, map, Observable, tap } from 'rxjs';
import { Account } from '../models/Account';
import { User } from '../models/User';
import { Router } from '@angular/router';
import { AccountService } from '../sevices/account.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  model: any = {}
  error: any = "";
  admin: boolean = true;

  constructor(public accountService: AccountService, private router: Router) { }

  ngOnInit(): void {
    this.admin = !!this.accountService.isAdmin();
  }

  checkLoggedin() {
    !!this.accountService.isLoggedIn()
  }

  logout() {
    this.accountService.logout();
    this.router.navigate(['login'])
  }
}
