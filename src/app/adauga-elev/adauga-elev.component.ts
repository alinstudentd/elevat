import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ElevNou } from '../models/ElevNou';
import { Profile } from '../models/Profile';
import { AccountService } from '../sevices/account.service';

@Component({
  selector: 'app-adauga-elev',
  templateUrl: './adauga-elev.component.html',
  styleUrls: ['./adauga-elev.component.css']
})
export class AdaugaElevComponent implements OnInit {
  elevN: object = {};

  elevNou = new FormGroup({
    nume: new FormControl('',  Validators.required),
    prenume: new FormControl('', Validators.required),
    role: new FormControl('', Validators.required),
    varsta: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    clasa: new FormControl('', Validators.required),
    liceul: new FormControl('', Validators.required),
    telefon: new FormControl('', Validators.required),
    adresa: new FormControl('', Validators.required)
  });

  constructor(private accountService: AccountService,
              private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.accountService.adaugaProfile(this.elevNou.value);
    this.router.navigate(['/elevi'])
  }
}
