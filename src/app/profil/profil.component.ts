import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Profile } from '../models/Profile';
import { AccountService } from '../sevices/account.service';
import { ActivatedRoute } from "@angular/router";
import { ElevServiceService } from '../sevices/elev-service.service';
import { Elev } from '../models/Elev';
import { ElevNou } from '../models/ElevNou';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  id: string | undefined;

  elevNou = new FormGroup({
    nume: new FormControl('',  Validators.required),
    prenume: new FormControl('', Validators.required),
    role: new FormControl('', Validators.required),
    varsta: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    clasa: new FormControl('', Validators.required),
    liceul: new FormControl('', Validators.required),
    telefon: new FormControl('', Validators.required),
    adresa: new FormControl('', Validators.required)
  });

  loggedIn: boolean = false;
  constructor(private accountService: AccountService,
             private route: ActivatedRoute,
             private elevService: ElevServiceService) { }

  ngOnInit(): void {
    this.accountService.getProfil()
        .subscribe( (profil ) => this.elevNou.patchValue(profil) )

  }

  onSubmit() {
    this.elevNou.value.id = this.id;
    this.accountService.updateProfile(this.elevNou.value);
  }
}
