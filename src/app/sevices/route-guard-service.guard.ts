import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RouteGuardServiceGuard implements CanActivate {
  public canActivate(route: ActivatedRouteSnapshot){
    let user = localStorage.getItem('user');

    if (user) {
      let user_role = JSON.parse(user).user;
      if (user_role.role == 'admin') {
        return true;
      }
    }

    return false;
  }

}
