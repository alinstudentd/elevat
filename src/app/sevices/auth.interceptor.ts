import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpClient
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  intercept(req: { clone: (arg0: { headers: any; }) => any;
  headers: { set: (arg0: string, arg1: string) => any; }; },
  next: { handle: (arg0: any) => any; }) {
      var user = localStorage.getItem('user');
      if (user) {
        var token = JSON.parse(user).token;
      }
      var authRequest = req.clone({
        headers: req.headers.set('Authorization', `Bearer ${token}`)
      })
      return next.handle(authRequest);
  }
}
