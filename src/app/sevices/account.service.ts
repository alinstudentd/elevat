import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { catchError, map, Observable, ReplaySubject } from 'rxjs';
import { Account } from '../models/Account';
import { ElevNou } from '../models/ElevNou';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  loginUrl = 'http://localhost/api/login';
  registerUrl = 'http://localhost/api/profile/register';
  updateUrl = 'http://localhost/api/profile/update';

  currentUserSource = new ReplaySubject<User>(1);
  currentUser$ = this.currentUserSource.asObservable();
  model: Account | undefined;
  userAuth: any = {};

  constructor(private http: HttpClient,
    private toastr: ToastrService,
    private router: Router) { }

  canActivate(): boolean {
    if (!this.isLoggedIn()) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }

  updateProfile(model: any) {
    this.http.post<any>('http://localhost/api/profile/update', model).subscribe(response => {
      return response
    })
  }

  adaugaProfile(model: any) {
    this.http.post<any>(this.registerUrl, model).subscribe(response => {
      return response
    })
  }

  getProfil(): Observable<ElevNou[]> {
    return this.http.get<ElevNou[]>('http://localhost/api/profile/showauth');
  }

  register(model:  any) {
    this.http.post<any>(this.registerUrl, model).subscribe(response => {
      return response
    })
  }

  login(model:  User) {
    return this.http.post(this.loginUrl, model).pipe(
      map((response: any) => {
        const user = response;
        if (user) {
          this.setCurrentuser(user);
          this.router.navigate(['/'])
        }
      })
    )
  }

  setCurrentuser(user: User) {
    localStorage.setItem('user', JSON.stringify(user));
    this.currentUserSource.next(user);
  }

  isLoggedIn() {
    return !!localStorage.getItem('user');
  }

  isElev() {
    let userAccount  = localStorage.getItem('user');
    if ( userAccount ) {
      return JSON.parse(userAccount).user.role === 'elev';
    }
    return false;
  }

  getUserID() {
    let userAccount  = localStorage.getItem('user');
    if ( userAccount) {
      return JSON.parse(userAccount).user.id;
    }
  }
  username() {
    let userAccount  = localStorage.getItem('user');
    if ( userAccount) {
      return JSON.parse(userAccount).user.username;
    }
  }

  isAdmin() {
    let userAccount  = localStorage.getItem('user');
    if (  userAccount) {
      return JSON.parse(userAccount).user.role === 'admin';
    }
    return false;
  }

  logout() {
    return localStorage.removeItem('user');
  }
}

