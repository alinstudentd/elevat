import { Injectable } from '@angular/core';
import { Account } from '../models/Account';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, Observer, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { User } from '../models/User';
import { Elev } from '../models/Elev';
import { Invite } from '../models/Invite';
import { Meet } from '../models/Meet';

@Injectable({
  providedIn: 'root'
})
export class InteractionsService {

  constructor(
  private http: HttpClient) { }

  private InteractAPI = 'http://localhost/api/interaction';  // URL to web api
  private InvitationsAPI = 'http://localhost/api/invite';
  invite(elev: any): void {
    this.http.post<Elev>(this.InteractAPI, {to: elev.id}).subscribe(response => {
      return response;
    })
  }

  meet(): Observable<Meet> {
    return this.http.get<any[]>(this.InteractAPI).pipe(
      map((response: any) => {
       return response
      }, (error: any) => {
        return error;
      })
    );
  }
}

