import { Injectable } from '@angular/core';
import { Account } from '../models/Account';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Elev } from '../models/Elev';
import { ElevNou } from '../models/ElevNou';
import { Event } from '../models/Event';

@Injectable({
  providedIn: 'root'
})

export class ElevServiceService {

  constructor(private http: HttpClient) { }

  private EleviAPI = 'http://localhost/api/elev/';  // URL to web api
  private ProfileAPI = 'http://localhost/api/profile';
  private ProfileAPIUpdate = 'http://localhost/api/profile/update';
  private GetProfileAPI = 'http://localhost/api/profile/showauth';
  private eventsAPI = 'http://localhost/api/events'

  addEvent(model:  any) {
    this.http.post<any>(this.eventsAPI, model).subscribe(response => {
      return response
    })
  }

  listaEvenimente(): Observable<Event[]> {
    return this.http.get<Event[]>(this.eventsAPI);
  }

  listaElevi(): Observable<ElevNou[]> {
    return this.http.get<ElevNou[]>(this.EleviAPI);
  }

  listaProfile(): Observable<ElevNou[]> {
    return this.http.get<ElevNou[]>(this.ProfileAPI);
  }

  getProfil(): Observable<ElevNou[]> {
    return this.http.get<ElevNou[]>(this.GetProfileAPI);
  }

  getEvent(id: any): Observable<Event[]> {
    return this.http.get<Event[]>('http://localhost/api/events/'+id);
  }

  updateEvent(model: any) {
    this.http.put<any>(this.eventsAPI+'/'+model.id, model).subscribe(response => {
      return response
    })
  }
  updateElev(model: any) {
    this.http.post<any>(this.ProfileAPIUpdate, model).subscribe(response => {
      return response
    })
  }
  getElev(id: any): Observable<ElevNou[]> {
    return this.http.get<ElevNou[]>('http://localhost/api/profile/'+id);
  }

  goingToParty( model: any) {
    this.http.post<any>(this.eventsAPI+'/participa/', model).subscribe(response => {
      return response
    })
  }
}

