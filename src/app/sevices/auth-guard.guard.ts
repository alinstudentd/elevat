import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { map, Observable } from 'rxjs';
import { AccountService } from './account.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
  constructor(private accountService: AccountService,
              private router: Router) {
  }

  canActivate(): any {
    if (!this.accountService.isLoggedIn()) {
      this.router.navigate(['login']);
    }
    return true;
  }

}
