import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AccountService } from './sevices/account.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Elevat';
  role: boolean = false;
  loggedIn: boolean = false;
  userAccount: any;
  constructor(private http: HttpClient, private accountService: AccountService)
  {

  }
  ngOnInit(): void {
    this.loggedIn = this.accountService.isLoggedIn();
    this.role = this.accountService.isAdmin();
  }
}
