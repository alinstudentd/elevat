import { Component, OnInit } from '@angular/core';
import { delay, map, Observable, tap } from 'rxjs';
import { Router } from '@angular/router';
import { AccountService } from '../sevices/account.service';
import { ToastrService } from 'ngx-toastr';
import { FormControl, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loggedIn: boolean = false;
  model: any = {}
  error: any = "";
  email: any;

  constructor(private accountService: AccountService,
     private router: Router,
     private toastr: ToastrService) { }

     loginForm = new FormGroup({
      username: new FormControl('',  Validators.required),
      password: new FormControl('', Validators.required),
    });

    ngOnInit(): void {
  }

  showError(message: string){
    this.toastr.error(message)
  }

  onSubmit() {
    // this.accountService.login(this.loginForm.value).pipe(
    //   delay(100),
    //   tap(() => this.router.navigate(['/']),
    // )).subscribe();
    this.accountService.login(this.loginForm.value).subscribe();
  }
}
