import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ElevNou } from '../models/ElevNou';
import { Profile } from '../models/Profile';
import { AccountService } from '../sevices/account.service';
import { ActivatedRoute } from "@angular/router";
import { ElevServiceService } from '../sevices/elev-service.service';

@Component({
  selector: 'app-editare-eveniment',
  templateUrl: './editare-eveniment.component.html',
  styleUrls: ['./editare-eveniment.component.css']
})

export class EditareEvenimentComponent implements OnInit {
  event: object = {};

  editareEvent = new FormGroup({
    id: new FormControl(),
    eveniment: new FormControl('',  Validators.required),
    locatie: new FormControl('', Validators.required),
    data: new FormControl('', Validators.required),
    pret: new FormControl('', Validators.required)
  });

  constructor(private accountService: AccountService,
              private router: Router,
              private elevService: ElevServiceService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
     const id = this.route.snapshot.paramMap.get('id') || undefined;
     if (id) {
      this.elevService.getEvent(id)
      .subscribe( (event ) => this.editareEvent.patchValue(event) )
     }

    }

  onSubmit() {
    const id = this.route.snapshot.paramMap.get('id') || undefined;
    this.editareEvent.value.id = id;
    this.elevService.updateEvent(this.editareEvent.value);
    this.router.navigate(['/evenimente'])
  }
}
