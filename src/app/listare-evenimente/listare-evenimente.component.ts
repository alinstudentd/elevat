import { Component, OnInit } from '@angular/core';
import { AccountService } from '../sevices/account.service';
import { ElevServiceService } from '../sevices/elev-service.service';

@Component({
  selector: 'app-listare-evenimente',
  templateUrl: './listare-evenimente.component.html',
  styleUrls: ['./listare-evenimente.component.css']
})
export class ListareEvenimenteComponent implements OnInit {

  events: any[] = [];
  isUserId: any;
  constructor(private elevService: ElevServiceService,
    private accountService: AccountService) { }

  ngOnInit(): void {
    this.isUserId = this.accountService.getUserID();
    this.loadingEvents();
  }

  loadingEvents() {
    this.elevService.listaEvenimente().subscribe((events ) => this.events = events )
  }

  going(event: any): void {
    this.elevService.goingToParty(event);
    this.loadingEvents();
  }
}
