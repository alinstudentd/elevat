import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Elev } from '../models/Elev';
import { AccountService } from '../sevices/account.service';
import { InteractionsService } from '../sevices/interactions.service';

@Component({
  selector: 'app-intalnire',
  templateUrl: './intalnire.component.html',
  styleUrls: ['./intalnire.component.css']
})
export class IntalnireComponent implements OnInit {

  meeting : any = {};
  adminRole : boolean = false;
  constructor(
    private http: HttpClient,
    private interactionsService: InteractionsService,
    private accountService: AccountService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
        this.interactionsService.meet()
    .subscribe((meeting) => this.meeting = meeting);
  }

  checkUserId(id: any) {
    return this.accountService.getUserID() === id;
  }

  cancel() {
    this.toastr.error("you cancel it");
  }

  accept() {
    this.toastr.success("Congratulations, you have a date.");
  }

  intalnire() : void {

  }
}
