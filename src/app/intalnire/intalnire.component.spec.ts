import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntalnireComponent } from './intalnire.component';

describe('IntalnireComponent', () => {
  let component: IntalnireComponent;
  let fixture: ComponentFixture<IntalnireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntalnireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntalnireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
