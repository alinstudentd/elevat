import { Component, OnInit } from '@angular/core';
import { ElevServiceService } from '../sevices/elev-service.service';

@Component({
  selector: 'app-evenimente',
  templateUrl: './evenimente.component.html',
  styleUrls: ['./evenimente.component.css']
})
export class EvenimenteComponent implements OnInit {

  events: any[] = [];
  constructor(private elevService: ElevServiceService) { }

  ngOnInit(): void {
    this.elevService.listaEvenimente().subscribe((events ) => this.events = events )
  }

}
