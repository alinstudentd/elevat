import { Component, OnInit } from '@angular/core';
import { AccountService } from '../sevices/account.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  model: any = {}
  success: string = "";
  constructor(private accountService: AccountService,) { }

  ngOnInit(): void {
  }

  register() {
    this.accountService.register(this.model);
    this.model = {};
    this.success = "You have a new account, now you can login."
  }
}
