import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Elev } from '../models/Elev';
import { ElevNou } from '../models/ElevNou';
import { ElevServiceService } from '../sevices/elev-service.service';
import { InteractionsService } from '../sevices/interactions.service';

@Component({
  selector: 'app-elevi',
  templateUrl: './elevi.component.html',
  styleUrls: ['./elevi.component.css']
})
export class EleviComponent implements OnInit {

  constructor(
    private http: HttpClient,
    private elevService: ElevServiceService,
    private interactionsService: InteractionsService) { }

  elevi: ElevNou[] = [];
  id: Number | undefined;

  ngOnInit(): void {
    this.ListaElevi();
  }

  ListaElevi() : void{
    this.elevService.listaElevi().subscribe((elevi ) => this.elevi = elevi);
  }


  invite(id: any) : void {
    this.interactionsService.invite(id);
  }
}
