import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ElevServiceService } from '../sevices/elev-service.service';

@Component({
  selector: 'app-adauga-eveniment',
  templateUrl: './adauga-eveniment.component.html',
  styleUrls: ['./adauga-eveniment.component.css']
})
export class AdaugaEvenimentComponent implements OnInit {

  event: object = {};

  adaugaEvent = new FormGroup({
    eveniment: new FormControl('',  Validators.required),
    locatie: new FormControl('', Validators.required),
    data: new FormControl('', Validators.required),
    pret: new FormControl('', Validators.required)
  });
  constructor(private elevService: ElevServiceService,
              private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.elevService.addEvent(this.adaugaEvent.value);
    this.router.navigate(['/evenimente'])
  }

}
