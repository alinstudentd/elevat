import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NavComponent } from './nav/nav.component';
import { AuthInterceptor } from '../app/sevices/auth.interceptor';
import { RegisterComponent } from './register/register.component';
import { IntalnireComponent } from './intalnire/intalnire.component';
import { ProfilComponent } from './profil/profil.component';
import { GrupComponent } from './grup/grup.component';
import { LoginComponent } from './login/login.component';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { CommonModule } from '@angular/common';
import { EleviComponent } from './elevi/elevi.component';
import { AdaugaElevComponent } from './adauga-elev/adauga-elev.component';
import { ProfileComponent } from './profile/profile.component';
import { EditareElevComponent } from './editare-elev/editare-elev.component';
import { EvenimenteComponent } from './evenimente/evenimente.component';
import { AdaugaEvenimentComponent } from './adauga-eveniment/adauga-eveniment.component';
import { ListareEvenimenteComponent } from './listare-evenimente/listare-evenimente.component';
import { FooterComponent } from './footer/footer.component';
import { EditareEvenimentComponent } from './editare-eveniment/editare-eveniment.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    RegisterComponent,
    EleviComponent,
    IntalnireComponent,
    ProfilComponent,
    GrupComponent,
    LoginComponent,
    AdaugaElevComponent,
    EditareElevComponent,
    EditareEvenimentComponent,
    ProfileComponent,
    EvenimenteComponent,
    AdaugaEvenimentComponent,
    ListareEvenimenteComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    }),
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
