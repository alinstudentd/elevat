import { Component, OnInit } from '@angular/core';
import { ElevNou } from '../models/ElevNou';
import { ElevServiceService } from '../sevices/elev-service.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  profile: ElevNou[] = [];

  constructor(private elevService: ElevServiceService,) { }

  ngOnInit(): void {
    this.elevService.listaProfile().subscribe((profile ) => this.profile = profile);
  }

}
