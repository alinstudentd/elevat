import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdaugaElevComponent } from './adauga-elev/adauga-elev.component';
import { AdaugaEvenimentComponent } from './adauga-eveniment/adauga-eveniment.component';
import { EditareElevComponent } from './editare-elev/editare-elev.component';
import { EditareEvenimentComponent } from './editare-eveniment/editare-eveniment.component';
import { EleviComponent } from './elevi/elevi.component';
import { EvenimenteComponent } from './evenimente/evenimente.component';
import { IntalnireComponent } from './intalnire/intalnire.component';
import { ListareEvenimenteComponent } from './listare-evenimente/listare-evenimente.component';
import { LoginComponent } from './login/login.component';
import { ProfilComponent } from './profil/profil.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuardGuard } from './sevices/auth-guard.guard';
import { RouteGuardServiceGuard } from './sevices/route-guard-service.guard';

const appRoutes: Routes = [
  { path: '', component: ListareEvenimenteComponent, canActivate: [AuthGuardGuard]},
  { path: 'login', component: LoginComponent },
  { path: 'evenimente', component: EvenimenteComponent, canLoad:[RouteGuardServiceGuard], canActivate: [AuthGuardGuard]},
  { path: 'adauga-eveniment', component: AdaugaEvenimentComponent, canLoad:[RouteGuardServiceGuard], canActivate: [AuthGuardGuard]},
  { path: 'editare-eveniment/:id', component: EditareEvenimentComponent, canActivate: [RouteGuardServiceGuard]},
  { path: 'profil', component: ProfilComponent, canLoad:[RouteGuardServiceGuard], canActivate: [AuthGuardGuard]},
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuardGuard]},
  { path: 'elevi', component: EleviComponent, canActivate: [RouteGuardServiceGuard]},
  { path: 'adauga-elev', component: AdaugaElevComponent, canActivate: [RouteGuardServiceGuard]},
  { path: 'editare-elev/:id', component: EditareElevComponent, canActivate: [RouteGuardServiceGuard]},
  { path: 'intalnire', component: IntalnireComponent, canActivate:[AuthGuardGuard]},
  { path: 'profil/:id', component: ProfilComponent, canActivate: [AuthGuardGuard] },
  { path: '**', redirectTo: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
