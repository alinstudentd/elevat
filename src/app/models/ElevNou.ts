export interface ElevNou {
   id: number;
   email: string;
   nume: string;
   prenume: string;
   photo: any,
   user: any;
   photos: any,
   role: string;
   varsta: number;
   clasa: string;
   liceul: string;
   telefon: string;
   adresa: string;
}
