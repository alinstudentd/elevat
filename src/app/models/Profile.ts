export class Profile {
  private id!: string;
  private email!: string;
  private nume!: string;
  private prenume!: string;
  private varsta!: number;
  private adresa!: string;
  private rol!: string;
  private clasa!: string;
  private liceul!: string;
  private telefon!: string;
}
