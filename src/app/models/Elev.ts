export interface Elev {
  id: number,
  username: string;
  lastname: string;
  email: string;
  name: string;
  surname: string;
  picture: string;
  age: number;
  photos: any;
}
